require 'rails/generators/base'

module Praetor
  class SetupGenerator
    source_root File.expand_path('templates', __dir__)

    def create_migration
      timestamp = Time.now.utc.strftime('%Y%m%d%H%M%S')
      template 'create_praetor_models.erb', "db/migrate/create_praetor_models.rb.erb"
    end

    def create_email_views
      template 'password_reset_email.txt.erb.erb', 'app/views/email/password_reset.txt.erb'
    end

    def create_sms_views
      template 'password_reset_sms.txt.erb.erb', 'app/views/sms/password_reset.txt.erb'
    end
  end
end
