require 'jwt'

module Praetor
  class Auth
    ALGORITHM = 'HS256'

    class AuthError < StandardError
    end

    def self.encode(payload)
      JWT.encode(payload, Rails.application.config.auth['key'], ALGORITHM)
    end

    def self.decode(token)
      JWT.decode(token, Rails.application.config.auth['key'], true, { algorithm: ALGORITHM }).first
    rescue JWT::VerificationError
      nil
    end
  end
end
