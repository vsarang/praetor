module Praetor
  module ControllerConcerns
    module ErrorHelpers
      extend ActiveSupport::Concern

      included do
        rescue_from ActionController::ParameterMissing, with: :render_parameter_missing_error
        rescue_from ActiveRecord::RecordInvalid, with: :render_invalid_error
        rescue_from ActiveRecord::RecordNotFound, with: :render_not_found_error
        rescue_from ActiveRecord::RecordNotUnique, with: :render_not_unique_error
        rescue_from Praetor::Permissions::PermissionError, with: :render_permission_error
        rescue_from Praetor::Auth::AuthError, with: :render_auth_error

        before_action :validate_auth, only: [:create, :update, :destroy]
      end

      def validate_auth
        if auth_user.present?
          true
        else
          raise Praetor::Auth::AuthError
        end
      end

      def render_error(status, options)
        render(
          status: status,
          json: {
            message: options[:message],
            errors: options[:errors]
          }
        )
      end

      def render_auth_error
        render_error(401, message: 'invalid credentials')
      end

      def render_permission_error(error)
        render_error(403, message: error.message)
      end

      def render_not_found_error
        render_error(404, message: 'not found')
      end

      def render_parameter_missing_error(error)
        render_error(422, errors: {
          "#{error.param}": 'can\'t be blank'
        })
      end

      def render_not_unique_error
        render_error(422, message: 'already exists')
      end

      def render_invalid_error(error)
        render_error(422, errors: error.record.errors)
      end
    end
  end
end
