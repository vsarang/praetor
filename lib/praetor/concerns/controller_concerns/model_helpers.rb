module Praetor
  module ControllerConcerns
    module ModelHelpers
      extend ActiveSupport::Concern

      module ClassMethods
        def model(**options)
          @model_name = options[:name].pluralize
          if options[:name].end_with?('Base')
            collection_name = "filtered_#{options[:name].split('::').first.tableize}"
          else
            collection_name = "filtered_#{options[:name].tableize.singularize.split('/').reverse.join('_').pluralize}"
          end
          self.define_method(collection_name) do |**method_options|
            scope = options[:class].where(nil)
            scope = instance_exec(scope, &options[:scope]) if options[:scope].present?
            permission_user.filter_records(scope)
          end
        end

        def model_name
          @model_name
        end
      end

      def render_record(record, **options)
        render(
          json: record,
          scope: serializer_scope,
          **options
        )
      end

      def render_records(records, **options)
        render(
          json: records,
          scope: serializer_scope,
          **options
        )
      end

      def set_content_range(**options)
        headers['Content-Range'] = "#{self.class.model_name} #{options[:range_start]}-#{options[:range_end]}/#{options[:total_count]}"
      end

      def paginate_records(scope, **options)
        offset = Integer(params[:offset] || 0)
        total_count = options[:total_count] || scope.count
        limit = options[:limit] || 10

        set_content_range(
          range_start: offset,
          range_end: [offset + limit, total_count].min,
          total_count: total_count
        )

        if options[:reverse]
          scope = scope.order(id: :asc)
        else
          scope = scope.order(id: :desc)
        end

        render(
          json: options[:array] || scope.limit(limit).offset(offset).to_a,
          scope: serializer_scope,
          **(options[:render_options] || {})
        )
      end

      def serializer_scope
        {
          auth_user: auth_user,
          permission_user: permission_user
        }
      end
    end
  end
end
