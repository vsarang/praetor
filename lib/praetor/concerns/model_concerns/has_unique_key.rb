module Praetor
  module ModelConcerns
    module HasUniqueKey
      extend ActiveSupport::Concern
      include ActiveModel::Validations

      included do
        before_validation :assign_unique_key, on: :create
      end

      def assign_unique_key(length = 8)
        while key.blank? || self.class.base_class.find_by(key: key)
          self.key = SecureRandom.urlsafe_base64(length)
        end
      end
    end
  end
end
