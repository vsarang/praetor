module Praetor
  module ModelConcerns
    module JsonAttributes
      extend ActiveSupport::Concern
      include ActiveModel::Validations

      included do
        validate :validate_values
      end

      module ClassMethods
        def json_attribute(json_attribute)
          json_attribute_parts = json_attribute.to_s.split('_')
          if json_attribute_parts.last == 'json'
            attribute = json_attribute_parts[0..-2].join('_')
            unless method_defined?(attribute)
              define_method(attribute) do
                json_values(json_attribute)
              end
            end

            unless method_defined?("#{attribute}=")
              define_method("#{attribute}=") do |values|
                set_json(json_attribute, values)
              end
            end
          end
        end

        def json_value(json_attribute, key, type, **options)
          json_attribute(json_attribute)
          @json_attribute_configs ||= {}
          @json_attribute_configs[json_attribute] ||= {}
          @json_attribute_configs[json_attribute][key] = {
            type: type.constantize,
            required: options[:required]
          }
        end

        def json_attribute_configs(record)
          @json_attribute_configs
        end
      end

      def reload
        super
        @json_values = {}
      end

      def json_values(json_attribute)
        @json_values ||= {}
        if @json_values[json_attribute].nil?
          json_values = JSON.parse(self.send(json_attribute) || '{}')
          json_values = json_values.deep_symbolize_keys if json_values.is_a?(Hash)
          @json_values[json_attribute] = json_values
        end
        @json_values[json_attribute].clone
      end

      def assign_json_value(json_attribute, key, value)
        assign_json(json_attribute, "#{key}": value)
      end

      def assign_json(json_attribute, new_values)
        @json_values ||= {}
        @json_values[json_attribute] = {
          **json_values(json_attribute),
          **new_values
        }
        self[json_attribute] = @json_values[json_attribute].to_json
      end

      def set_json(json_attribute, values)
        @json_values ||= {}
        @json_values[json_attribute] = values
        if values.nil?
          self[json_attribute] = nil
        else
          self[json_attribute] = @json_values[json_attribute].to_json
        end
      end

      private

      def validate_values
        self.class.json_attribute_configs(self)&.each do |json_attribute, json_attribute_config|
          json_attribute_config.each do |key, validation_config|
            json_value = json_values(json_attribute)[key]
            if json_value.nil?
              errors.add(json_attribute, "#{key} must be set") if validation_config[:required]
            elsif !json_value.is_a?(validation_config[:type])
              if validation_config[:type] == Date
                begin
                  Praetor::Datetime.parse_date(json_value)
                rescue ArgumentError
                  errors.add(json_attribute, "#{key} must be Date")
                end
              elsif validation_config[:type] == DateTime
                begin
                  DateTime.iso8601(json_value)
                rescue ArgumentError
                  errors.add(json_attribute, "#{key} must be DateTime")
                end
              else
                errors.add(json_attribute, "#{key} must be #{validation_config[:type].to_s}")
              end
            end
          end
        end
      end
    end
  end
end
