module Praetor
  module ModelConcerns
    module PreserveCase
      extend ActiveSupport::Concern
      include ActiveModel::Validations

      CLASSES = []

      included do
        before_save :fix_fk_case
      end

      module ClassMethods
        def preserve_case(association, **options)
          PreserveCase::CLASSES.push(self)
          @preserve_case_config = {
            association: association,
            foreign_key: options[:foreign_key] || "#{association}_key",
            primary_key: options[:primary_key] || 'key'
          }
        end

        def preserve_case_config
          @preserve_case_config
        end
      end

      def fix_fk_case(**options)
        if (options[:force] || (
          self.class.preserve_case_config &&
          changed.include?(self.class.preserve_case_config[:foreign_key].to_s) &&
          send(self.class.preserve_case_config[:foreign_key])
        ))
          record = send(self.class.preserve_case_config[:association])
          if record
            primary_key_value = record.send(self.class.preserve_case_config[:primary_key])
            send("#{self.class.preserve_case_config[:foreign_key]}=", primary_key_value)
          end
        end
      end
    end
  end
end
