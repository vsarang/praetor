module Praetor
  module ModelConcerns
    module Restrictable
      extend ActiveSupport::Concern

      included do
        scope :query_as, -> (user, **options) {
          scope = where(options)
          user.filter_records(scope)
        }
      end

      module ClassMethods
        def permission_context(key = nil, **options)
          if key
            @permission_context_key ||= key
            @permission_context_class ||= options[:class]
          else
            @permission_context_class || self
          end
        end

        def permission_context_key
          @permission_context_key
        end

        def belongs_to_owner
          column_names.include?('owner_key')
        end

        def restrictable_options(user, **options)
          if (
            !user.new_record? &&
            belongs_to_owner &&
            !options.has_key?(:owner) &&
            !options.has_key?(:owner_key)
          )
            options.merge(owner: user)
          else
            options
          end
        end

        def create_as_on_scope(user, scope, **options)
          record = scope.new(restrictable_options(user, **options))
          raise ActiveRecord::RecordInvalid.new(record) if record.invalid?
          user.check_permission(record, :can_create)

          record.save!
          record
        end

        def create_as(user, **options)
          create_as_on_scope(user, where(nil), **options)
        end

        def find_as(user, **options)
          record = find_by(**options)
          user.check_permission(record) unless record.nil?
          record
        end

        def find_as!(user, **options)
          record = find_by!(**options)
          user.check_permission(record)
          record
        end
      end

      def permission_context
        if self.class.permission_context_key
          return send(self.class.permission_context_key)&.permission_context || self
        else
          self
        end
      end

      def has_permission_as_owner(user)
        return !new_record? && (
          (self.class == User && user == self) ||
          (self.class.column_names.include?('owner_key') && owner_key == user.key)
        )
      end

      def update_as(user, **options)
        if !has_permission_as_owner(user)
          user.check_permission(self, :can_edit)
        end

        update!(**options)
      end

      def destroy_as(user)
        if !has_permission_as_owner(user)
          user.check_permission(self, :can_destroy)
        end

        destroy!
      end

      def soft_delete_as(user)
        if !has_permission_as_owner(user)
          user.check_permission(self, :can_destroy)
        end

        soft_delete
      end

      def restrictable
        return true
      end
    end
  end
end
