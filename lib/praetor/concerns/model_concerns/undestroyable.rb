module Praetor
  module ModelConcerns
    module Undestroyable
      extend ActiveSupport::Concern

      included do
        if column_names.include?('archived')
          scope :unarchived, -> { where(archived: false) }
          scope :archived, -> { where(archived: true) }

          validate :validate_undestroyable, if: :archived
        end

        if column_names.include?('deleted')
          scope :undeleted, -> { where(deleted: false) }
          scope :deleted, -> { where(deleted: true) }

          validate :validate_undestroyable, if: :deleted
        end

        before_destroy { throw(:abort) }
      end

      module ClassMethods
        def undestroyable(*symbols)
          @undestroyable ||= []

          if symbols.any?
            @undestroyable.push(*symbols)
          else
            @undestroyable
          end
        end
      end

      def undestroyable_changed?
        self.class.undestroyable.any? { |symbol| send("#{symbol.to_s}_changed?") }
      end

      def soft_delete
        self.try(:before_soft_delete)

        if self.class.column_names.include?('archived')
          update!(archived: true)
        elsif self.class.column_names.include?('deleted')
          update!(deleted: true)
        end
      end

      private

      def validate_undestroyable
        if self.class.column_names.include?('archived')
          if archived_was && undestroyable_changed?
            errors.add(:base, 'record has been archived')
          end
        elsif self.class.column_names.include?('deleted')
          if deleted_was && undestroyable_changed?
            errors.add(:base, 'record has been deleted')
          end
        end
      end
    end
  end
end
