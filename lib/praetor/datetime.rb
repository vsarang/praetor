module Praetor
  class Datetime
    DATE_FORMAT = '%Y-%m-%d'
    TIME_FORMAT = '%H:%M:%S'

    def self.parse_date(string)
      Date.strptime(string, Praetor::Datetime::DATE_FORMAT)
    end

    def self.parse_datetime(date, time = nil)
      if date.is_a?(Date)
        date_string = date.strftime(Praetor::Datetime::DATE_FORMAT)
      else
        date_string = date
      end

      if time.is_a?(Time)
        time_string = time.strftime(Praetor::Datetime::TIME_FORMAT)
      else
        time_string = time
      end

      if date_string.present?
        if time_string.present?
          DateTime.iso8601("#{date_string}T#{time_string}Z")
        else
          DateTime.iso8601("#{date_string}T00:00:00Z")
        end
      end
    end

    def self.strip_timezone(datetime, timezone:)
      Time.zone.parse(
        datetime.in_time_zone(timezone).strftime('%c')
      ).to_datetime
    end

    def self.with_timezone(datetime, timezone:)
      Time.find_zone(timezone).parse(
        datetime.strftime('%c')
      ).to_datetime
    end
  end
end
