require 'nokogiri'
require_relative '../document_reader.rb'

module Praetor
  module Document
    module CSV
      SPLIT_REGEX = /,(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)/

      class EOPError < StandardError
      end

      class Base < ::Nokogiri::XML::SAX::Document
        @has_header = true

        def self.header(has_header = nil)
          if has_header.nil?
            @has_header
          else
            @has_header = has_header
          end
        end

        def self.from_file(path, **options)
          document_reader = DocumentReader.open(path, {
            start_pos: options[:start_pos],
            end_pos: options[:end_pos],
            start_pattern: "\n"
          })
          document_reader.read if header # skip \n character

          csv_document = new(document_reader, **options)
          document_reader.stream do |line|
            if header
              values = {}
              line.split(Praetor::Document::CSV::SPLIT_REGEX).each_with_index do |value, index|
                key = csv_document.keys[index]
                values[:"#{key}"] = value.strip.gsub(/\n/, '').gsub(/(^"|"$)/, '').strip.gsub(/\s+/, ' ')
              end
            else
              values = []
              line.split(Praetor::Document::CSV::SPLIT_REGEX).each_with_index do |value, index|
                values[index] = value.strip.gsub(/\n/, '').gsub(/(^"|"$)/, '').strip.gsub(/\s+/, ' ')
              end
            end

            csv_document.values(values)
          end
        rescue Document::CSV::EOPError
          document_reader.close_file
        end

        def initialize(document_reader, **options)
          @document_reader = document_reader
          @keys = document_reader.first_line.split(
            Praetor::Document::CSV::SPLIT_REGEX
          ).map { |key| key.strip } if self.class.header
        end

        def keys
          @keys
        end

        def document_reader
          @document_reader
        end
      end
    end
  end
end
