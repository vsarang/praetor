module Praetor
  class DocumentReader
    class IndexOutOfBoundsError < StandardError
    end

    class PatternNotFoundError < StandardError
      def initialize(pattern)
        super("#{pattern} not found")
      end
    end

    def self.open(path, **options)
      document_reader = new(::File.open(path))

      start_pos = options[:start_pos]&.to_f
      start_pos = document_reader.file_size * (start_pos / 100.0) if options[:start_pos]&.end_with?('%')
      start_pos = start_pos.to_i if start_pos.present?

      end_pos = options[:end_pos]&.to_f
      end_pos = document_reader.file_size * (end_pos / 100.0) if options[:end_pos]&.end_with?('%')
      end_pos = end_pos.to_i if end_pos.present?

      document_reader.end_pos(end_pos) if end_pos.present?
      document_reader.seek(start_pos) if start_pos.present?

      begin
        document_reader.rewind_to(options[:start_pattern])
      rescue DocumentReader::PatternNotFoundError
        document_reader.skip_to(options[:start_pattern])
      end

      document_reader
    end

    def initialize(file)
      @file = file
    end

    def first_line
      if @first_line.nil?
        cur_pos = pos
        seek(0)
        @first_line = @file.readline
        seek(cur_pos)
      end

      @first_line
    end

    def close_file
      @file.close
    end

    def file
      @file
    end

    def file_basename
      @basename ||= File.basename(@file.path)
      @basename
    end

    def start_pos
      @start_pos
    end

    def seek(to_pos)
      raise DocumentReader::IndexOutOfBoundsError if to_pos < 0
      file.seek(to_pos)
    end

    def pos
      file.pos
    end

    def end_pos(end_pos = nil)
      if end_pos.present?
        @end_pos = end_pos
      else
        @end_pos || file_size
      end
    end

    def file_size
      if @file_size.nil?
        @file_size = file.size
      end
      @file_size
    end

    def peek(n = 1)
      cur_pos = pos
      result = read(n)
      seek(cur_pos)
      result
    end

    def stream(&block)
      @start_pos = pos
      file.each_line(&block)
    end

    def read(n = 1)
      cur_pos = pos
      final_pos = cur_pos + n
      left_pos = [cur_pos, final_pos].min
      seek(left_pos)
      result = n.abs.times.map { file.readchar }.join
      seek(final_pos)
      result
    end

    def rewind_to(pattern)
      cur_pos = pos

      buffer = ''
      until (pos + pattern.length) <= file_size && pattern.start_with?(buffer) && head?(pattern)
        buffer = read(-1) + buffer
        buffer = buffer[0..(pattern.length - 1)] if buffer.length > pattern.length
      end
    rescue DocumentReader::IndexOutOfBoundsError
      seek(pos)
      raise DocumentReader::PatternNotFoundError, pattern
    end

    def skip_to(pattern)
      cur_pos = pos

      buffer = ''
      until pos >= pattern.length && pattern.end_with?(buffer) && tail?(pattern)
        buffer = buffer + read(1)
        buffer = buffer[-pattern.length..-1] if buffer.length > pattern.length
      end
      seek(pos - pattern.length)
    rescue EOFError
      seek(pos)
      raise DocumentReader::PatternNotFoundError, pattern
    end

    def head?(pattern)
      peek(pattern.length) == pattern
    end

    def tail?(pattern)
      peek(-pattern.length) == pattern
    end

    def processed_size
      pos - start_pos
    end

    def read_size
      end_pos - start_pos
    end

    def start_report
      @start_time = DateTime.now
    end

    def report
      total_elapsed_days = (DateTime.now - @start_time).to_f

      progress_ratio = processed_size.to_f / read_size
      days_elapsed = (DateTime.now - @start_time).to_f
      days_remaining = (days_elapsed / progress_ratio) * (1 - progress_ratio)

      hours_remaining = days_remaining * 24
      minutes_remaining = hours_remaining * 60
      seconds_remaining = minutes_remaining * 60

      if seconds_remaining < 60
        message = "#{seconds_remaining.round(0)}s"
      elsif minutes_remaining < 60
        message = "#{minutes_remaining.round(0)}m"
      elsif hours_remaining < 24
        message = "#{hours_remaining.round(1)}h"
      else
        message = "#{days_remaining.round(2)}d"
      end

      progress_percent = progress_ratio * 100
      "#{message} (#{progress_percent.round(2)}%)"
    end
  end
end
