require 'nokogiri'
require_relative '../document_reader.rb'

module Praetor
  module Document
    module XML
      class ReadError < StandardError
      end

      class EOPError < StandardError
      end

      class Base < ::Nokogiri::XML::SAX::Document
        def self.define_node(name, **options)
          @nodes ||= {}
          @nodes[name] = options
        end

        def self.root_node(name = nil, **options)
          if name.present?
            @root_node_name = name
            define_node(name, **options)
          else
            @root_node_name
          end
        end

        def self.node(name, **options)
          define_node(name, **options)
        end

        def self.nodes
          @nodes
        end

        def self.from_file(path, **options)
          document_reader = DocumentReader.open(path, {
            start_pos: options[:start_pos],
            end_pos: options[:end_pos],
            start_pattern: "<#{root_node} "
          })

          xml_document = new(document_reader, **options)
          push_parser = ::Nokogiri::XML::SAX::PushParser.new(xml_document)
          push_parser << '<releases>'
          document_reader.stream do |line|
            push_parser << line
          end
          document_reader.close_file
        rescue Document::XML::ReadError => error
          document_reader.close_file
          raise Document::XML::ReadError, "Position #{document_reader.pos}\n  #{error.message}"
        rescue Document::XML::EOPError
          document_reader.close_file
        end

        def initialize(document_reader)
          @document_reader = document_reader
          @node_stack = []
          @element_stack = []
          @value = nil
        end

        def start_element(name, attributes = [])
          @element_stack.push(name)

          if (
            (node.nil? && name == self.class.root_node) ||
            (node.present? && node['nodes'].key?(name))
          )
            node_config = self.class.nodes[name]
            new_node = {}
            new_node['name'] = name
            new_node['attributes'] = {}
            new_node['values'] = {}
            new_node['nodes'] = {}

            attributes_hash = attributes.to_h
            node_config[:attributes]&.each do |attribute_key|
              new_node['attributes'][attribute_key] = attributes_hash[attribute_key]
            end

            node_config[:values]&.each do |value_key|
              new_node['values'][value_key] = nil
            end

            node_config[:nodes]&.each do |child_node_key|
              child_node_config = self.class.nodes[child_node_key]
              raise "missing definition for <#{child_node_key}>" if child_node_config.nil?
              if child_node_config[:multiple]
                new_node['nodes'][child_node_key] = []
              else
                new_node['nodes'][child_node_key] = nil
              end
            end

            push_node(new_node)

            if node_config[:value]
              node['value'] = ''
            end
          elsif value_belongs_to_node?
            @value = ''
          end
        end

        def characters(string)
          if !@value.nil?
            @value += string
          elsif node.present? && !node['value'].nil?
            node['value'] += string
          end
        end

        def end_element(name)
          if @value.present?
            node['values'][name] = @value
            @value = nil
          elsif node.present? && node['name'] == name
            pop_node
          end

          @element_stack.pop
        end

        def error(message)
          raise Document::XML::ReadError, message
        end

        private

        def node
          @node_stack.last
        end

        def parent_node
          @node_stack[-2]
        end

        def value_belongs_to_node?
          node.present? && node['name'] == @element_stack[-2] && node['values'].key?(@element_stack.last)
        end

        def push_node(node)
          @node_stack.push(node)
        end

        def pop_node
          if parent_node.present?
            node_config = self.class.nodes[node['name']]
            if node_config[:multiple]
              parent_node['nodes'][node['name']].push(node)
            else
              parent_node['nodes'][node['name']] = node
            end
          elsif node['name'] == self.class.root_node
            root_node(node)
          end

          @node_stack.pop
        end

        def document_reader
          @document_reader
        end
      end
    end
  end
end
