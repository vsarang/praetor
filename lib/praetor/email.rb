module Praetor
  class Email
    attr_accessor :scope

    def self.send(options)
      Praetor::Email.new(options[:scope]).send(options.except(:scope))
    end

    def initialize(scope)
      self.scope = scope
    end

    def send(options)
      email = SendGrid::Mail.new(
        SendGrid::Email.new(email: Rails.application.config.email['default_from_email']),
        options[:subject],
        SendGrid::Email.new(email: options[:to_email]),
        SendGrid::Content.new(
          type: 'text/plain',
          value: ERB.new(
            File.read("app/views/email/#{options[:template]}.erb")
          ).result(binding)
        )
      )

      send_grid_api = SendGrid::API.new(api_key: Rails.application.config.email['sendgrid_api_key'])
      send_grid_api.client.mail._('send').post(request_body: email.to_json)
    end
  end
end
