module Praetor
  class Sms
    attr_accessor :scope

    def self.normalize_phone(phone)
      if phone.nil?
        nil
      else
        phone = phone.to_s.gsub(Praetor::Regex::PHONE_REJECT_REGEX, '')
        if phone.starts_with?('1')
          "+#{phone}"
        else
          "+1#{phone}"
        end
      end
    end

    def self.send(options)
      Praetor::Sms.new(options[:scope]).send(options.except(:scope))
    end

    def initialize(scope)
      self.scope = scope
    end

    def send(options)
      twilio_client = Twilio::REST::Client.new(
        Rails.application.config.sms['twilio_account_sid'],
        Rails.application.config.sms['twilio_auth_token']
      )

      twilio_client.messages.create(
        from: Rails.application.config.sms['twilio_from_phone'],
        to: options[:to_phone],
        body: ERB.new(
          File.read("app/views/sms/#{options[:template]}.erb")
        ).result(binding)
      )
    end
  end
end
