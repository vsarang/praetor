namespace :praetor do
  namespace :db do
    task fix_fk_case: :environment do
      puts 'Fixing foreign key case...'
      # Load all record classes to populate PreserveCase::CLASSES
      Rails.application.eager_load!
      PreserveCase::CLASSES.each do |klass|
        puts "-> #{klass.to_s}"
        klass.all.each do |record|
          record.fix_fk_case(force: true)
          if record.changed?
            record.save!
          end
        end
      end
      puts ''
    end
  end
end
