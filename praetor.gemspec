Gem::Specification.new do |s|
  s.name = 'praetor'
  s.version = '0.0.43'
  s.summary = 'Praetor'
  s.authors = ['Vikrant Sarang']
  s.email = ['vikrantsarang@gmail.com']
  s.files = [
    'lib/praetor.rb',
    *Dir[File.join(__dir__, 'lib/{generators,tasks}/**/*')]
  ]
end
